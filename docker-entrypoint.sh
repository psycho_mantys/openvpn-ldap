#!/bin/bash

set -e

export OPENVPN_CONF=${OPENVPN_CONF:-'/etc/openvpn/default.ovpn'}
export OPENVPN_PORT=${OPENVPN_PORT:-'10000'}
export OPENVPN_MGMT_PORT=${OPENVPN_MGMT_PORT:-'8080'}
export OPENVPN_MGMT_PASSWORD=${OPENVPN_MGMT_PASSWORD:-''}
export OPENVPN_AUTH_PORT=${OPENVPN_AUTH_PORT:-'9000'}

function do_gosu() {
	user="$1"
	shift 1

	is_exec="false"
	if [ "$1" = "exec" ]; then
		is_exec="true"
		shift 1
	fi

	if [ "$(id -u)" = "0" ]; then
		if [ "${is_exec}" = "true" ]; then
			exec gosu "${user}" "$@"
		else
			gosu "${user}" "$@"
			return "$?"
		fi
	else
		if [ "${is_exec}" = "true" ]; then
			exec "$@"
		else
			eval '"$@"'
			return "$?"
		fi
	fi
}

parse_env(){
	if [ -r "$1" ] ; then
		while IFS="=" read key value  ; do
			export "${key}=${value}"
		done<<<"$( egrep '^[^#]+=.*' "$1" )"
	fi
}

bootstrap_conf(){
	if [ "${TZ}" ] ; then
		ln -snf "/usr/share/zoneinfo/${TZ}" /etc/localtime && echo "${TZ}" > /etc/timezone
	fi

	find /etc/openvpn -writable -a -not -user "${APP_UID}" -exec chown "${APP_UID}:${APP_GID}" {} \+
}

parse_env '/env.sh'
parse_env '/run/secrets/env.sh'

bootstrap_conf

if [[ "start" == "$1" ]]; then
	if [ -r "${OPENVPN_CONF}.tmpl" ] ; then
		envsubst < "${OPENVPN_CONF}.tmpl" > "${OPENVPN_CONF}"
		exec openvpn --config "${OPENVPN_CONF}" <<<"${OPENVPN_MGMT_PASSWORD}"
	elif [ -r "${OPENVPN_CONF}" ] ; then
		exec openvpn --config "${OPENVPN_CONF}" <<<"${OPENVPN_MGMT_PASSWORD}"
	elif [ "${OPENVPN_CONF}" ] ; then
		echo "${OPENVPN_CONF}" | envsubst > "/tmp/default.ovpn"
		exec openvpn --config "/tmp/default.ovpn" <<<"${OPENVPN_MGMT_PASSWORD}"
	fi
	exit 0
elif [[ "start-openvpn-auth-oauth2" == "$1" ]]; then
	do_gosu "${APP_UID}:${APP_GID}" exec /bin/openvpn-auth-oauth2
elif [[ "init" == "$1" ]]; then
	mkdir -p /dev/net
	mknod /dev/net/tun c 10 200
	chmod 600 /dev/net/tun
	exit 0
elif [[ "init-firewall" == "$1" ]]; then
	iptables -t nat -I POSTROUTING 1 -s "${OPENVPN_LAN}" -o eth0 -j MASQUERADE
	iptables -I INPUT 1 -i tun0 -j ACCEPT
	iptables -I FORWARD 1 -i eth0 -o tun0 -j ACCEPT
	iptables -I FORWARD 1 -i tun0 -o eth0 -j ACCEPT
	iptables -I INPUT 1 -i eth0 -p udp --dport "${OPENVPN_PORT}" -j ACCEPT
	exit 0
elif [[ "healthcheck" == "$1" ]]; then
	if [[ "openvpn" == "$2" ]]; then
		exec nc -q 20 -w 5 127.0.0.1 "${OPENVPN_MGMT_PORT}" <<<"status"
	elif [[ "openvpn-auth-oauth2" == "$2" ]]; then
		exec nc -q 20 -w 5 127.0.0.1 "${OPENVPN_AUTH_PORT}"
	fi
	exit 0
elif [[ "make-server" == "$1" ]]; then
	if [ ! -f /etc/openvpn/default/pki/ca.crt ]; then
		/usr/share/easy-rsa/easyrsa init-pki nopass
		/usr/share/easy-rsa/easyrsa build-ca nopass
		/usr/share/easy-rsa/easyrsa build-server-full default nopass
	fi
	if [ ! -f /etc/openvpn/default/ta.key ]; then
		openvpn --genkey tls-crypt-v2-server /etc/openvpn/default/ta.key
		openvpn --tls-crypt-v2 /etc/openvpn/default/ta.key --genkey tls-crypt-v2-client /etc/openvpn/default/ta-client.key
	fi

	exit 0
elif [[ "make-client" == "$1" ]]; then
	/usr/share/easy-rsa/easyrsa build-server-full "$2" nopass

	exit 0
fi

exec "$@"

# vim: nu ts=4 noet ft=bash:
