FROM debian:bookworm-slim AS base

# Env and Args
ARG APP_UID=999
ENV APP_UID=${APP_UID}
ARG APP_GID=999
ENV APP_GID=${APP_GID}
#ARG OPENVPN_GIT_BRANCH="release/2.6"

LABEL openvpn="master" \
    os="debian" \
    os.version="12" \
    name="openvpn-ldap" \
    description="Openvpn with ldap support" \
    maintainer="Psycho Mantys"

ARG BUILD_PACKAGES="\
  sudo openvpn autoconf re2c libtool \
  libldap2-dev libssl-dev gobjc make \
  wget ca-certificates libldap-2.5-0 libsasl2-2"

ENV BUILD_PACKAGES=${BUILD_PACKAGES}

ARG RUNTIME_PACKAGES="\
  gosu easy-rsa openvpn iptables \
  openssl ca-certificates curl \
  netcat-traditional libobjc4 \
  gettext-base"

ENV RUNTIME_PACKAGES=${RUNTIME_PACKAGES}

RUN addgroup --gid "${APP_GID}" openvpn \
  && useradd --system -m -d / -u "${APP_UID}" -g "${APP_GID}" openvpn

FROM base AS build

RUN apt-get update \
 && apt-get install -y --no-install-recommends ${BUILD_PACKAGES} \
 && wget -O master.tar.gz https://github.com/threerings/openvpn-auth-ldap/archive/master.tar.gz \
 && tar -xzf master.tar.gz \
 && cd openvpn-auth-ldap-master \
 && ./regen.sh \
 && ./configure --prefix=/usr --with-openvpn=/usr/include/openvpn CFLAGS="-fPIC" OBJCFLAGS="-std=gnu11" \
 && make \
 && mkdir -p /install/usr/lib /install/etc \
 && make DEST=/install DESTDIR=/install install
 #&& mkdir /openvpn \
 #&& cd /openvpn \
 #&& git clone --depth 1 https://github.com/OpenVPN/openvpn.git --branch $${OPENVPN_GIT_BRANCH} --single-branch . \
 #&& autoreconf -i -v -f \
 #&& ./configure --prefix=/usr \
 #&& make \
 #&& make install

FROM golang:1.23-bookworm AS build-openvpn-auth-oauth2

RUN go install github.com/jkroepke/openvpn-auth-oauth2@latest

FROM base

COPY --from=build /install /

RUN apt-get update \
 && SUDO_FORCE_REMOVE=yes apt-get remove --purge -y ${BUILD_PACKAGES} \
 && apt-get autoremove -y \
 && rm -rf /usr/share/man \
 && apt-get install -y --no-install-recommends ${RUNTIME_PACKAGES} \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && mkdir -p /etc/openvpn/default/tmp /etc/openvpn/default/easyrsa

VOLUME /etc/openvpn/default/tmp
VOLUME /etc/openvpn/default/easyrsa

COPY --from=build-openvpn-auth-oauth2 /go/bin/openvpn-auth-oauth2 /bin
COPY docker-entrypoint.sh /
COPY default.conf /etc/openvpn/
COPY auth-ldap.conf /etc/openvpn/default/
COPY ovpn-unify.py /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["start"]
#CMD ["sleep", "2h"]

HEALTHCHECK --interval=90s --timeout=5s --start-period=1m \
  CMD /docker-entrypoint.sh healthcheck openvpn

